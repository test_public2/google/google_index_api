"""
https://www.jcchouinard.com/google-indexing-api-with-python/
pip install oauth2client httplib2
"""


import json
import urllib.parse

import httplib2
from oauth2client.service_account import ServiceAccountCredentials

'''
Batch Requests Endpoint
To send batch request to the indexing API, make POST requests to this API endpoint:
https://indexing.googleapis.com/batch
'''
'''
{'error': {'code': 429, 'message': "Quota exceeded for quota metric 'Publish requests' and limit 'Publish requests per day' of service 'indexing.googleapis.com' for consumer 'project_number:311392580877'.", 'status': 'RESOURCE_EXHAUSTED', 'details': [{'@type': 'type.googleapis.com/google.rpc.ErrorInfo', 'reason': 'RATE_LIMIT_EXCEEDED', 'domain': 'googleapis.com', 'metadata': {'quota_limit_value': '200', 'quota_location': 'global', 'quota_limit': 'DefaultPublishRequestsPerDayPerProject', 'consumer': 'projects/311392580877', 'quota_metric': 'indexing.googleapis.com/v3_publish_requests', 'service': 'indexing.googleapis.com'}}, {'@type': 'type.googleapis.com/google.rpc.Help', 'links': [{'description': 'Request a higher quota limit.', 'url': 'https://cloud.google.com/docs/quota#requesting_higher_quota'}]}]}}
'''


class GoogleIndexingApi:
    def __init__(self, json_key_file):
        auth_url = ["https://www.googleapis.com/auth/indexing"]
        self.REQUEST_URL = "https://indexing.googleapis.com/v3/urlNotifications:publish"
        self.STATUS_URL = 'https://indexing.googleapis.com/v3/urlNotifications/metadata'
        credentials = ServiceAccountCredentials.from_json_keyfile_name(json_key_file, scopes=auth_url)
        self.http = credentials.authorize(httplib2.Http())

    def request_update(self, url):
        content = {'url': url, 'type': 'URL_UPDATED'}
        json_content = json.dumps(content)
        response, content = self.http.request(self.REQUEST_URL, method="POST", body=json_content)
        result = json.loads(content.decode())
        return result

    def request_delete(self, url):
        content = {'url': url, 'type': 'URL_DELETED'}
        json_content = json.dumps(content)
        response, content = self.http.request(self.REQUEST_URL, method="POST", body=json_content)
        result = json.loads(content.decode())
        return result

    def request_status(self, url):
        response, content = self.http.request(uri=f'{self.STATUS_URL}?url={urllib.parse.quote(url)}', method="GET")
        # response, content = self.http.request(uri=f'{STATUS_POINT}?url={url}', method="GET")
        result = json.loads(content.decode())
        return result


def main():
    google_api = GoogleIndexingApi(json_key_file='kramnychka-1598781374099-88fa1bc94ab4.json')

    # url = 'https://www.kramnychka.icu/ru/knitwear/knitted_suit/N040293?limit=75'

    while True:
        url = input('url:')
        response = google_api.request_status(url)
        print(response)
        # response = google_api.request_update(url)
        # print(response)
        response = google_api.request_delete(url)
        print(response)
        response = google_api.request_status(url)
        print(response)
        print('#########################################################################')


if __name__ == '__main__':
    main()
